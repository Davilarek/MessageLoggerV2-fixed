# MessageLogger V2 [<a target="_blank" download="MessageLoggerV2.plugin.js" href="https://gitea.slowb.ro/Davilarek/MessageLoggerV2-fixed/raw/branch/master/Plugins/MessageLoggerV2/MessageLoggerV2.plugin.js">Download</a>] [<a target="_blank" download="1XenoLib.plugin.js" href="https://gitea.slowb.ro/Davilarek/MessageLoggerV2-fixed/raw/branch/master/Plugins/1XenoLib.plugin.js">Download XenoLib</a>] [![Hits](https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2Fdavilarek.github.io%2FMessageLoggerV2-fixed%2Fdownload&count_bg=%2379C83D&title_bg=%23555555&icon=&icon_color=%23E7E7E7&title=Download+count&edge_flat=true)](https://hits.seeyoufarm.com)
#### Info
This repository is a fork of https://github.com/1Lighty/BetterDiscordPlugins
<br>
MessageLoggerV2 and XenoLib were created by Lighty. I just fixed it.

Make sure to backup your data file before using my version.

If you require support for any of his plugins, join his Discord server (can be found in original readme).

All of his plugins are available for easy downloading on his website.

## What's fixed:
- Edits
- "Open logs"
- Message removed/edited notification???
- Saving images to image cache directory
- Context menus
- Color of "Remove From Log"
- Image cache server (alternative implementation)

## What's to be fixed:
- Memory leaks
- https://gitea.slowb.ro/Davilarek/MessageLoggerV2-fixed/issues/5 (?)

## What's custom:
- Support for embed images (thanks for idea @Ansemik_CZE)
- Saving images to directory structure main_folder/server_name/channel_name/file_name ("Use new cached images system" in settings) (thanks for idea @Ansemik_CZE)
- Some other stuff I don't remember
- Notification whitelist (thanks for idea @Voleno1, https://gitea.slowb.ro/Davilarek/MessageLoggerV2-fixed/issues/3)
- Deleted Message Colors (thanks @imafrogowo, https://gitea.slowb.ro/Davilarek/MessageLoggerV2-fixed/pulls/29)
- Option to save all deleted attachments (thanks @imafrogowo, https://gitea.slowb.ro/Davilarek/MessageLoggerV2-fixed/pulls/29, note, this isn't fully https://gitea.slowb.ro/Davilarek/MessageLoggerV2-fixed/issues/23)
